'use strict';

var gulp = require('gulp'),
    ts = require('gulp-typescript'),
    watch = require('gulp-watch'),
    clean = require('gulp-clean'),
    tsOptions = require('./tsconfig.json'),
    sass = require('gulp-sass'),
    replace = require('gulp-replace');

    delete tsOptions.compilerOptions.buildOnSave;

// Compile *.ts to *.js
gulp.task( 'compile', ['clear'] , function () {
  return gulp.src([ 'src/**/*.ts' ])
    .pipe( ts(tsOptions.compilerOptions) )
    .pipe( gulp.dest('./dist') );
});

// Copy static dependencies to dist/vendor
gulp.task('vendors', function() {
   return gulp.src([
    'node_modules/core-js/client/shim.min.js' ,
    'node_modules/zone.js/dist/zone.js',
    'node_modules/reflect-metadata/Reflect.js',
    'node_modules/systemjs/dist/system.src.js'
  ])
  .pipe( gulp.dest('./dist/vendor') );
});


// Clear compiled JS files
gulp.task('clear', function() {
   return gulp.src( [ 'dist/**/*.*' , '!dist/vendor/*.js', '!dist/css/foundation.min.css' ], { read: false } )
   .pipe(clean());
});


// Copy assets files to dist/
gulp.task('copy-assets', ['compile'], function() {
   return gulp.src([ 'src/**/*.*' , '!src/**/*.ts', '!src/**/*.scss' ])
   .pipe( gulp.dest('./dist') );
});

// Watch
gulp.task('watch', function() {
  gulp.watch(
    ['src/**/*.*'] , [ 'default' ]
  );
});

gulp.task( 'default' , [ 'compile' , 'copy-assets', 'clear' ] );
