import { Component, Input } from "@angular/core";

@Component({
    selector: "usersTable",
    templateUrl: "dist/common/components/usersTable/ut.template.html"
})

export class UsersTableComponent {

  @Input() users: Array<any> = [];

}
