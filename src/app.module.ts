import { NgModule }             from '@angular/core';
import { BrowserModule }        from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule }           from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent }   from './app.component';
import { HelloComponent } from './hello/hello.component';
import { FooComponent }   from './foo/foo.component';
import { HttpComponent }  from './http/http.component';
import { FormComponent }  from './form/form.component';
import { UsersTableComponent }  from './common/components/usersTable/ut.component';

const appRoutes: Routes = [
    { path: 'foo', component: FooComponent },
    { path: 'http', component: HttpComponent },
    { path: 'form', component: FormComponent },
    { path: '', component: HelloComponent }
];

@NgModule({
    imports: [
        BrowserModule,
        RouterModule.forRoot(appRoutes, { useHash: true }),
        HttpModule,
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [],
    declarations: [
        AppComponent,
        FooComponent,
        HelloComponent,
        HttpComponent,
        FormComponent,
        UsersTableComponent
    ],
    bootstrap: [ AppComponent ]
})

export class AppModule { }
