import { Component } from '@angular/core';

@Component({
  template: `
    <h1>Hello {{name}}</h1>
    <span [routerLink]="['/foo']">Foo Component</span> <br>
    <span [routerLink]="['/http']">Http Component</span> <br>
    <span [routerLink]="['/form']">Form Component</span> 
    `,
})
export class HelloComponent  {

  name: string = 'Hello component!';

}
