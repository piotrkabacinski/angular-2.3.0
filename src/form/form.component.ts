import { Component } from "@angular/core";
import { FormBuilder, Validators } from '@angular/forms';
// import { fooValidator } from "../form/fooValidator";

@Component({
    templateUrl: "dist/form/form.template.html",

})

export class FormComponent {

    private sampleForm: any;
    private formResults: any = {
        show: false
    };
    private title: string = "Formularz";

    constructor(
        private formBuilder: FormBuilder
    ) {

        this.sampleForm = this.formBuilder.group({
            // "input": ["", Validators.compose([fooValidator.checkFoo])],
            "input": ['', Validators.compose([Validators.required])],
            "textarea": [ "Default value", Validators.compose([Validators.required])],
            "checkbox": [],
            "select": []
        });

    }

    displayFormResults() {

        let resultsHolder = this.sampleForm.controls;

        this.formResults = {
            input: resultsHolder.input.value,
            textarea: resultsHolder.textarea.value,
            checkbox: resultsHolder.checkbox.value,
            select: resultsHolder.select.value,
            show: true
        };

    }

}
