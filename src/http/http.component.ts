import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import { UsersTableComponent } from '../common/components/usersTable/ut.component';
import 'rxjs/Rx'

@Component({
    templateUrl: "dist/http/http.template.html"
})

export class HttpComponent {

    private users: Array<string> = [];

    constructor(
      private http: Http
    ) { }

    get() {

        this.http.get( "dist/http/mocks/example.json" )
                 .map(response => response.json())
                 .subscribe(
                   data => {
                       this.users = data;
                       console.log( data );
                   },
                   error => {
                       console.log(error);
                   }
                 );
    }

}
